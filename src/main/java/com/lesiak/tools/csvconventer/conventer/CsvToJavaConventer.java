package com.lesiak.tools.csvconventer.conventer;

import com.lesiak.tools.csvconventer.pojo.CsvDataObject;
import net.sf.jsefa.Deserializer;
import net.sf.jsefa.csv.CsvIOFactory;
import net.sf.jsefa.csv.config.CsvConfiguration;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class CsvToJavaConventer {

    public static List<CsvDataObject> getDataListFromCsvFile(String filePath) throws IOException {
        List<CsvDataObject> csvDataObjectList = new LinkedList<>();

        CsvConfiguration csvConfiguration = new CsvConfiguration();
        csvConfiguration.setFieldDelimiter(',');

        Deserializer deserializer =
                CsvIOFactory.createFactory(csvConfiguration, CsvDataObject.class).createDeserializer();

        InputStream inputStream = getInputStream(filePath);

        deserializer.open(new InputStreamReader(inputStream));

        while (deserializer.hasNext()) {
            CsvDataObject csvDataObject = deserializer.next();
            String brand = csvDataObject.getBrand();
            boolean isVehicleInListAlready = csvDataObjectList.contains(csvDataObject);

            if (!brand.contains("make") && !isVehicleInListAlready) {
                csvDataObjectList.add(csvDataObject);
            }
        }

        deserializer.close(true);

        return csvDataObjectList;
    }

    private static InputStream getInputStream(String filePath) throws IOException {
        File initialFile = new File(filePath);
        return new FileInputStream(initialFile);
    }

}
