package com.lesiak.tools.csvconventer.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ParticularVehicle {
    private String brand;
    private String model;
    private String version;
    private String engineCode;
    private boolean confirmed = true;

    public ParticularVehicle(String brand, String model, String version, String engineCode) {
        this.brand = brand;
        this.model = model;
        this.version = version;
        this.engineCode = engineCode;
    }
}
