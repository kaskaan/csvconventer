package com.lesiak.tools.csvconventer.pojo;

import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

import java.util.List;
import java.util.Objects;
//@Data  tu mozesz sobie lomboka uzyc
@CsvDataType
public class CsvDataObject {

    @CsvField(pos = 1)
    private String brand;

    @CsvField(pos = 2)
    private String model;

    @CsvField(pos = 3)
    private List<String> version;

    @CsvField(pos = 4)
    private List<String> engineCode;

    private boolean confirmed = true;

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public List<String> getVersion() {
        return version;
    }

    public List<String> getEngineCode() {
        return engineCode;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    @Override
    public String toString() {
        return "CsvDataObject{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", version=" + version +
                ", engineCode=" + engineCode +
                ", confirmed=" + confirmed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CsvDataObject that = (CsvDataObject) o;
        return Objects.equals(brand, that.brand) &&
                Objects.equals(model, that.model) &&
                Objects.equals(version, that.version) &&
                Objects.equals(engineCode, that.engineCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, version, engineCode);
    }

}
