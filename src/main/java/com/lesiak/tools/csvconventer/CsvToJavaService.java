package com.lesiak.tools.csvconventer;

import com.lesiak.tools.csvconventer.conventer.CsvToJavaConventer;
import com.lesiak.tools.csvconventer.pojo.ParticularVehicle;
import com.lesiak.tools.csvconventer.pojo.CsvDataObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Konrad Lesiak
 */

public class CsvToJavaService {

    private CsvToJavaService() {
    }

    public static List<ParticularVehicle> getListOfParticularVehicles (String filePath) throws IOException {
        List<CsvDataObject> csvDataObjectList = CsvToJavaConventer.getDataListFromCsvFile(filePath);

        List<ParticularVehicle> particularVehicleList = new ArrayList<>();

        for (CsvDataObject csvDataObject : csvDataObjectList) {
            String brand = csvDataObject.getBrand();
            String model = csvDataObject.getModel();
            List<String> version = csvDataObject.getVersion();
            List<String> engineCode = csvDataObject.getEngineCode();

            if (version != null && engineCode != null) {
                version.forEach(x -> engineCode.
                        forEach(y -> particularVehicleList.add(new ParticularVehicle(brand, model, x, y))));
            }
            if (version != null && engineCode == null) {
                version.forEach(v -> particularVehicleList.add(new ParticularVehicle(brand, model, v, "null")));
            }
            if (version == null && engineCode != null) {
                engineCode.forEach(e -> particularVehicleList.add(new ParticularVehicle(brand, model, null, e)));
            }
            if (version == null && engineCode == null) {
                particularVehicleList.add(new ParticularVehicle(brand, model, null, null));
            }
        }
        return particularVehicleList;
    }

}
