package com.lesiak;

import com.lesiak.tools.csvconventer.pojo.ParticularVehicle;
import com.lesiak.tools.csvconventer.CsvToJavaService;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        int id = 1;
        //powinno wystarczyc bez /home/tnifey/IdeaProjects/csvconventer/
        final String filePath = "/home/tnifey/IdeaProjects/csvconventer/src/main/resources/cars.csv";

        List<ParticularVehicle> vehicleList = CsvToJavaService.getListOfParticularVehicles(filePath);

        for (ParticularVehicle aVehicleList : vehicleList) {
            System.out.println("ID: " + id + ' ' + aVehicleList);
            id++;
        }
    }
}
